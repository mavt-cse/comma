# create makefile rules

# ex
in ():

	a/main.c

out:

	-Ia

# ex
in ():

	,a/
	main.c

out:

	-I.
# ex
in (a):

	,a/
	,b/
	main.c

out:

	-I,a -I.

# ex
in (a, b):

	,a/
	,b/
	main.c

out:

	-I,a -I,b -I.

# ex: with global
in (b):

	g/,a
	g/,b
	,a/
	,b/
	main.c

out:

	-Ig/,b -I,b -I.

# ex: with global
in (a, b):

	g/,a/,b
	main.c

out:

	-I,a -I,a/,b

# impl
For headers:
A: absolute dir `src/,a/,b`
C: comma collapsed dir `src/` : remove trailing commas
P: comma prefix `,a/,b`

Do
Map[C].append(A)
Map[C].append(P)

For sources:
dump M[C] and M[g]
