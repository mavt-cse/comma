#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include <conf.h>
#include "inc/conf.h"
#include "os.h"
#include "m.h"     /* MPI */
#include "l/m.h"
#include "inc/type.h"
#include "io/rbc.h"

#include "mc.h"
#include "io/rbc/imp.h"
