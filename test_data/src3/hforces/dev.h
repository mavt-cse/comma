namespace hforces {
namespace dev {

struct Pa { /* local particle */
    float x, y, z;
    float vx, vy, vz;
    uint id;
};

struct Fo { float *x, *y, *z; }; /* force */

static __device__ float fst(float2 p) { return p.x; }
static __device__ float scn(float2 p) { return p.y; }
static __device__ void p2rv2(const float2 *p, uint i,
                             float  *x, float  *y, float  *z,
                             float *vx, float *vy, float *vz) {
    float2 s0, s1, s2;
    p += 3*i;
    s0 = __ldg(p++); s1 = __ldg(p++); s2 = __ldg(p++);
     *x = fst(s0);  *y = scn(s0);  *z = fst(s1);
    *vx = scn(s1); *vy = fst(s2); *vz = scn(s2);
}

static __device__ Pa frag2p(const Frag frag, uint i) {
    Pa p;
    p2rv2(frag.pp, i, /**/ &p.x, &p.y, &p.z,   &p.vx, &p.vy, &p.vz);
    p.id = i;
    return p;
}

static __device__ void pair(const Pa l, const Pa r, float rnd, /**/ float *fx, float *fy, float *fz) {
    forces::dpd0(SOLVENT_TYPE, SOLVENT_TYPE,
                 l.x, l.y, l.z,
                 r.x, r.y, r.z,
                 l.vx, l.vy, l.vz,
                 r.vx, r.vy, r.vz,
                 rnd,
                 fx, fy, fz);
}

static __device__ float random(uint lid, uint rid, float seed, int mask) {
    uint a1, a2;
    a1 = mask ? lid : rid;
    a2 = mask ? rid : lid;
    return rnd::mean0var1uu(seed, a1, a2);
}

static __device__ void force0(const Rnd rnd, const Frag frag, const Map m, const Pa l, /**/
                              float *fx, float *fy, float *fz) {
    /* l, r: local and remote particles */
    Pa r;
    uint i;
    uint lid, rid; /* ids */
    float x, y, z; /* pair force */
    lid = l.id;    

    *fx = *fy = *fz = 0;
    for (i = threadIdx.x & 1; !endp(m, i); i += 2) {
        rid = m2id(m, i);
        r = frag2p(frag, rid);
        pair(l, r, random(lid, rid, rnd.seed, rnd.mask), &x, &y, &z);
        *fx += x; *fy += y; *fz += z;
    }
}

static __device__ void force1(const Rnd rnd, const Frag frag, const Map m, const Pa p, Fo f) {
    float x, y, z; /* force */
    force0(rnd, frag, m, p, /**/ &x, &y, &z);
    atomicAdd(f.x, x);
    atomicAdd(f.y, y);
    atomicAdd(f.z, z);
}

static __device__ void force2(const Frag frag, const Rnd rnd, Pa p, /**/ Fo f) {
    int dx, dy, dz;
    Map m;
    m = r2map(frag, p.x, p.y, p.z);

    dx = frag.dx; dy = frag.dy; dz = frag.dz; /* TODO: where it should be? */
    p.x -= dx * XS;
    p.y -= dy * YS;
    p.z -= dz * ZS;
    force1(rnd, frag, m, p, f);
}

static __device__ Fo i2f(const int *ii, float *ff, uint i) {
    /* local id and index to force */
    Fo f;
    ff += 3*ii[i];
    f.x = ff++; f.y = ff++; f.z = ff++;
    return f;
}

static __device__ void p2rv(const float *p, uint i,
                            float  *x, float  *y, float  *z,
                            float *vx, float *vy, float *vz) {
    p += 6*i;
     *x = *(p++);  *y = *(p++);  *z = *(p++);
    *vx = *(p++); *vy = *(p++); *vz = *(p++);
}

static __device__ Pa sfrag2p(const SFrag sfrag, uint i) {
    Pa p;
    p2rv(sfrag.pp,     i, /**/ &p.x, &p.y, &p.z,   &p.vx, &p.vy, &p.vz);
    p.id = i;
    return p;
}

static __device__ Fo sfrag2f(const SFrag sfrag, float *ff, uint i) {
    return i2f(sfrag.ii, ff, i);
}

static __device__ void force3(const SFrag sfrag, const Frag frag, const Rnd rnd, uint i, /**/ float *ff) {
    Pa p;
    Fo f;
    p = sfrag2p(sfrag, i);
    f = sfrag2f(sfrag, ff, i);
    force2(frag, rnd, p, f);
}

__global__ void force(const int27 start, const SFrag26 ssfrag, const Frag26 ffrag, const Rnd26 rrnd, /**/ float *ff) {
    Frag frag;
    Rnd  rnd;
    SFrag sfrag;
    unsigned int gid;
    uint h; /* halo id */
    uint i; /* particle id */

    gid = (threadIdx.x + blockDim.x * blockIdx.x) >> 1;
    if (gid >= start.d[26]) return;
    h = k_common::fid(start.d, gid);
    i = gid - start.d[h];
    sfrag = ssfrag.d[h];
    if (i >= sfrag.n) return;

    frag = ffrag.d[h];
    rnd = rrnd.d[h];
    force3(sfrag, frag, rnd, i, /**/ ff);
}

} // dev
} // hforces
