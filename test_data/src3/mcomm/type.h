namespace mcomm {

/* Structure containing all kinds of requests a fragment can have */
struct Reqs {
    MPI_Request pp[26], counts[26];
};

} // mcomm
