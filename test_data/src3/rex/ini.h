namespace rex {
static void i2d(int i, /**/ int d[3]) { /* fragment id to directiron */
    enum {X, Y, Z};
    d[X] = (i     + 2) % 3 - 1;
    d[Y] = (i / 3 + 2) % 3 - 1;
    d[Z] = (i / 9 + 2) % 3 - 1;
}

static int d2sz(int d[3]) { /* direction to size */
    enum {X, Y, Z};
    int x, y, z;
    x = (d[X] == 0 ? XS : 1);
    y = (d[Y] == 0 ? YS : 1);
    z = (d[Z] == 0 ? ZS : 1);
    return x * y * z;
}

static int i2sz(int i) { /* fragment id to size */
    int d[3];
    i2d(i, d);
    return d2sz(d);
}

static int i2max(int i) { /* fragment id to maximum size */
    return MAX_OBJ_DENSITY*i2sz(i);
}

static void ini_local() {
    int i, n;
    LocalHalo *h;
    for (i = 0; i < 26; i++) {
        n = i2max(i);
        h = &local[i];
        Dalloc(&h->indexes, n);

        Palloc0(&h->ff_pi, n);
        Link(&h->ff, h->ff_pi);
    }
}

static void ini_remote() {
    int i, n;
    RemoteHalo* h;
    for (i = 0; i < 26; i++) {
        n = i2max(i);
        h = &remote[i];
        Palloc0(&h->pp_pi, n);
        Link(&h->pp, h->pp_pi);

        Palloc0(&h->ff_pi, n);
        Link(&h->ff, h->ff_pi);
    }
}

static void ini_copy() {
    int i;
    for (i = 0; i < 26; ++i)
        CC(cudaMemcpyToSymbol(k_rex::g::indexes, &local[i].indexes, sizeof(int*), sizeof(int*) * i, H2D));
}

static void ini_ff() {
    int i;
    float *ff[26];
    for (i = 0; i < 26; ++i) ff[i] = (float*)local[i].ff;
    CC(cudaMemcpyToSymbolAsync(k_rex::g::ff, ff, sizeof(ff), 0, H2D));
}

void ini() {
    ini_local();
    ini_remote();
    ini_copy();
    ini_ff();
}
}
