namespace l { namespace ply {
void write(const char *fname, const Mesh m);
void read(const char *fname, Mesh *m);
}}
