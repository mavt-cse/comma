namespace l { namespace linal { /* linear algebra */
    void inv3x3(float *m0, /**/ float *minv0); /* invert symmetric matrix m0[6] */
}}
