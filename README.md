# comma

## notation

### conf
a list of activated dir names

    cpu
    rbc

### directories
* `,`: all dirs
* `+`: active
* `-`: not active
* list: all headers and sources

## [act]ive
in: conf, list
filter inactive coma dirs from a file list

## suffix
in: act
sorts files into sources and the rest

## obj
in: act
transforms sources to objects

## rule
in: suffix
see [rule](rule.md)

## dir 
in: suffix
print a rule to dirs

## dep
in: act, content of files
uses collapsed path, handle globals
