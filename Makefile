D = src

install:
	main () ( cd "$$d" ; make $$1); \
	for d in $D; do main install; done

test:; atest test/*

.PHONY: install test
